CREATE DATABASE db_castro;

CREATE TABLE tbl_users(
  id INT NOT NULL AUTO_INCREMENT,
  username VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE tbl_users_info (
  id INT NOT NULL AUTO_INCREMENT,
  user_id INT NOT NULL,
  name VARCHAR(45) NOT NULL,
  position VARCHAR(45) NOT NULL,
  email VARCHAR(45),
  address VARCHAR(45),
  company VARCHAR(45) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_info_user_id  
    FOREIGN KEY(user_id) REFERENCES tbl_users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);