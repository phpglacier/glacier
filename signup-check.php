<?php 
session_start(); 
include "db_conn.php";

if (isset($_POST['uname']) && isset($_POST['password'])
    && isset($_POST['name']) && isset($_POST['address']) && isset($_POST['email']) && isset($_POST['position']) && isset($_POST['company'])) {

	function validate($data){
       $data = trim($data);
	   $data = stripslashes($data);
	   $data = htmlspecialchars($data);
	   return $data;
	}

	$uname = validate($_POST['uname']);
	$pass = validate($_POST['password']);

	$name = validate($_POST['name']);
	$position = validate($_POST['position']);
	$address = validate($_POST['address']);
	$email = validate($_POST['email']);
	$company = validate($_POST['company']);

	$user_data = 'uname='. $uname. '&name='. $name;


	if (empty($uname)) {
		header("Location: signup.php?error=User Name is required&$user_data");
	    exit();
	}else if(empty($pass)){
        header("Location: signup.php?error=Password is required&$user_data");
	    exit();
	}

	else if(empty($name)){
        header("Location: signup.php?error=Name is required&$user_data");
	    exit();
	}


	else if(empty($position)){
        header("Location: signup.php?error=position is required&$user_data");
	    exit();
	}

	else if(empty($email)){
        header("Location: signup.php?error=Email is required&$user_data");
	    exit();
	}

	else if(empty($address)){
        header("Location: signup.php?error=Email is required&$user_data");
	    exit();
	}

	else if(empty($company)){
        header("Location: signup.php?error=Email is required&$user_data");
	    exit();
	}


	else{

				// hashing the password
		        $pass = md5($pass);

			    $sql = "SELECT * FROM users WHERE user_name='$uname' ";
				$result = mysqli_query($conn, $sql);

				if (mysqli_num_rows($result) > 0) {
					header("Location: signup.php?error=The username is taken try another&$user_data");
			        exit();
				}else if{
		           $sql2 = "INSERT INTO users(user_name, password) VALUES('$uname', '$pass')";
		           $result2 = mysqli_query($conn, $sql2);
		           if ($result2) {
		           	 header("Location: signup.php?success=Your account has been created successfully");
			         exit();
		           }else {
			           	header("Location: signup.php?error=unknown error occurred&$user_data");
				        exit();
		           } 
				} else {
					$sql3 = "INSERT INTO tbl_users_info(name, position, email, address, company) VALUES('$name', '$position', '$email', '$address', '$company')";
					$result3 = mysqli_query($conn, $sql3);
					if ($result3) {
					     header("Location: signup.php?success=Your account has been created successfully");
					   exit();
					} else {
			           	header("Location: signup.php?error=unknown error occurred&$user_data");
				        exit();
		           }
				}
			}
			
		}else{
			header("Location: signup.php");
			exit();
		}